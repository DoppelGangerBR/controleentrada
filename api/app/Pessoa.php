<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    protected $fillable  = ['id','observacao','acesso','tipo','codpessoa','telefone','celular','nome','ativo'];
    protected $hidden = ['created_at','deleted_at','updated_at','id'];
    protected $datas = ['deleted_at','created_at','updated_at'];
    protected $table = 'pessoa';

    public function pessoa(){
        return $this->hasMany('App\Pessoa');
    }
    
    public function pessoaxveiculo(){
        return $this->hasMany('App\pessoaxveiculo', 'codpessoa', 'codpessoa');
    }
    public function pessoaxapartamento(){
        return $this->hasMany('App\pessoaxapartamento', 'codpessoa', 'codpessoa');
    }
}
