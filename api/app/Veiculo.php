<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Veiculo extends Model
{
    protected $fillable  = ['modelo','cor','placa','ativo'];
    protected $hidden = ['created_at','deleted_at','updated_at','id'];
    protected $datas = ['deleted_at','created_at','updated_at'];
    protected $table = 'veiculo';

    public function veiculo(){
        return $this->hasMany('App\Veiculo');
    }
    
}
