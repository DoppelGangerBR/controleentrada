<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $fillable  = ['codevento','programado','dataprogramada','horaprogramada','datainicio','horainicio','datafim','horafim','observacao','sinistro','codpessoa','codveiculo','codap'];
    protected $hidden = ['id','created_at','updated_at','deleted_at','update_at'];
    protected $datas = ['deleted_at','created_at','updated_at'];
    protected $table = 'evento';

    public function evento(){
        return $this->hasMany('App\Evento');
    }
    public function pessoa(){
        return $this->hasOne('App\Pessoa','codpessoa','codpessoa');
    }
    public function veiculo(){
        return $this->hasOne('App\Veiculo','codveiculo','codveiculo');
    }
    public function apartamento(){
        return $this->hasOne('App\Apartamento','codap','codap');
    }
}
