<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Evento;
use App\Pessoa;
class EventoController extends Controller
{
    public function index(){
        $eventos = Evento::with('pessoa','veiculo','apartamento')->get();
        return response()->json($eventos);
    }
    public function store(Request $request){
        $dados = $request->all();
        try{
            $evento = Evento::create($dados);
            if($evento){
                return response()->json(['status'=>1]);
            }else{
                return response()->json(['status'=>0]);
            }
        }catch(\Exception $e){
            return response()->json(['status'=>0, $e]);
        }
    }
}
