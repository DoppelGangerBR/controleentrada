<?php

namespace App\Http\Controllers;

use DummyFullModelClass;
#use App\lain;
use App\Usuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests;
use JWTAuth;
use Hash;
use JWTGuard;
use Illuminate\Support\Facades\DB;

class AutorizacaoController extends Controller
{

    private function getToken($email, $senha){
        $token = null;
        try{
            if(!$token = JWTAuth::attempt(['email' => $email, 'senha' => $senha])){
                return response()->json([
                    'response' => 'erro',
                    'erro' => ['Email ou senha invalidos'],
                    'token' => $token,
                ]);
            }
        }catch(JWTAuthException $e){
            return response()->json([
                'response' => 'erro',
                'erros' => ['Falha ao criar token'],
            ]);
        }
        return $token;
    }

    public function VerificaSeEAdministrador(Request $request){
        $dados = $request->only(['token']);               
        $Administrador = false; 
        if(!$dados == null or !$dados == ''){
            $where = ['auth_token' => $dados['token']];                
            $Administrador = Usuario::select('superuser')->where($where)->get(); 
        }
        return response()->json($Administrador);
    }

    /*public function login(Request $request){
        $usuario = Usuario::where('email', $request->email)->get()->first();
        $senha = Hash::make($request->senha);

        if($usuario && Hash::check($request->senha, $request->senha)){
            $token  =  $self = self::getToken($request->email, $request->senha);
            if(!isset($token->original)){
                $usuario->auth_token = $token;
                $usuario->save();
                $response = ['status'=>true, 'CodUsuario'=>$usuario->CodUsuario, 'token'=> $usuario->auth_token, 'email'=> $usuario->email, 'superuser'=> $usuario->superuser];
            }else{
                $response = ['erro'=>'Usuario não encontrado'];
            }
            return response()->json($response, 201);
        }
    }*/
    
    public function ValidaToken(Request $request){
        $user_ret = false;
		$dados = $request->token;
		
		if(!$dados == null || !$dados == ''){
			$usuario = Usuario::where('auth_token', $request->token)->get()->first();
			if($usuario != null){
				$user_ret = true;
			}
        }
        return response()->json(['Autenticado'=>$user_ret],201);
		//return response()->json(['Autenticado'=>$dados],201);
    }
    public function Logout(){
        JWTAuth::invalidate();
        return response([
            'status'=>'sucesso',
            'msg' => 'Logout com sucesso'
        ],200);
    }
    public function Refresh(){
        return response([
            'status' => 'sucesso'
        ]);
    }
    
    public function login(Request $request)
    {
        $credenticiais = $request->only('usuario','senha');        
        $usuario = Usuario::where('usuario', $credenticiais['usuario'])->first();
        if(!$usuario){
            return response()->json([
                'Erro' => 'Usuario não encontrado'
            ], 401);            
        }
        if($usuario['ativo'] == false){
            return response()->json([
                'Erro' => 'Usuario desativado, por favor, contate-nos'
            ], 401);            
        }        
        if(!Hash::check($credenticiais['senha'], $usuario->senha)){
            return response()->json([
                'Erro' => 'Senha invalida!'
            ], 401);
        }        
        $token = JWTAuth::fromUser($usuario);
        $objectToken = JWTAuth::setToken($token);
        $usuario->auth_token = $token;
        $usuario->save();
        //$response = ['status'=>true, 'CodUsuario'=>$usuario->CodUsuario, 'token'=> $usuario->auth_token, 'email'=> $usuario->email, 'superuser'=> $usuario->superuser];
        $expiracao = JWTAuth::decode($objectToken->getToken())->get('exp');

        return response()->json([
            'token_acesso' => $token,
            'tipo_token' => 'bearer',
            'expires_in' => 3600
        ]);
    }

    public function authenticate(Request $request){
        try{
            if(! $usuario = JWTAuth::parseToken()->authenticate()){
                return response()->json(['Nao encontrado'],404);            
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        return response()->json(compact('usuario'));
    }
}

