<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Apartamento;
class ApartamentoController extends Controller
{
    public function index(){
        $apartamento = Apartamento::where('ativo','=',true)->get();
        return $apartamento;
    }
    public function store(Request $request){
        $dados = $request->all();
        try{
            $apartamento = Apartamento::create($dados);
            if($apartamento){
                return response()->json(['status'=>1]);
            }else{
                return response()->json(['status'=>0]);
            }
        }catch(\Exception $e){
            return response()->json(['status'=>0, $e]);
        }
    }
    public function alteraApartamento(Request $request){
        $dados = $request->all();
        $apartamento = Apartamento::where('codap',$dados['codap']);
        if($apartamento){
            $atualizaApartamento = $apartamento->update($dados);
            if($atualizaApartamento){
                return response()->json(['status'=>true]);
            }
        }else{
            return response()->json(['status'=>false]);
        }
    }

    public function desativaApartamento(Request $request){
        $status = false;
        $dados = $request->all();
        $codap = $dados['codap'];
        try{
            $apartamento = Apartamento::where('codap', '=', $codap);
            $desativa = $apartamento->update($dados);
            if($desativa){
                $status = true;
            }
            return response()->json(['status' => $status]);
        }catch(\Exception $e){
            return response()->json(['status' => $status, $e]);
        }
    }
}
