<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaPessoaxapartamento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pessoaxapartamento', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('codpessoa');
            $table->foreign('codpessoa')->references('codpessoa')->on('pessoa');
            $table->integer('codap');
            $table->foreign('codap')->references('codap')->on('apartamento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pessoaxapartamento');
    }
}
