/*
* Esse metodo serve para retornar todos os valores selecionados nos selects (veículo e apartamento)
* Deve se passar o ID do select como parametro, ele ira verificar todos os registros selecionados no select
* e ira coloca-los num array e então retornará os valores;
* o retorno é semelhante a isso ["22","5","14","3","10","7"] (Exemplo caso o usuário selecione 6 valores)
*/
function retornaMultiplosSelecionados(idElemento){
    const selected = document.querySelectorAll(idElemento+' option:checked');
    return Array.from(selected).map(el => el.value); 
}
